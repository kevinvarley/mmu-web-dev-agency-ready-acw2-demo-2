console.log("INFO: Starting app...\nWill listen on port 3000...");

// import express node framework
var app = require('express')();
var http = require('http').Server(app);
// import & initialize socket with the http object
var io = require('socket.io')(http);

var connectedUsers = 0;

// route web root
app.get('/', function(req, res){
  // send html web page to client
  res.sendFile(__dirname + '/index.html');
});


app.get('/assets/css/style.css', function(req, res) {
    res.sendFile(__dirname + '/assets/css/style.css');
});

io.on('connect', function(socket) {
  connectedUsers++;
  io.emit('updateconnectedusers', connectedUsers);
});

io.on('connection', function(socket) {
  var timestamp = getTime();
  var message = timestamp + ": INFO: A user connected";
  socket.broadcast.emit('message', message);

  socket.on('updateconnectedusers', function(connectedUsers) {
    io.emit('updateconnectedusers', connectedUsers);
  });

  socket.on('message', function(message) {
    var timestamp = getTime();
    message = timestamp + ": " + message;
    io.emit('message', message);
  });

  socket.on('disconnect', function() {
    connectedUsers--;
    io.emit('updateconnectedusers', connectedUsers);

    var timestamp = getTime();
    var message = timestamp + ": INFO: A user disconnected";
    socket.broadcast.emit('message', message);
  });
});

function getTime() {
  var dateObject = new Date();

  var hour = dateObject.getHours();

  hour = (hour < 10 ? "0" : "") + hour;

  var minutes = dateObject.getMinutes();
  minutes = (minutes < 10 ? "0" : "") + minutes;

  var seconds = dateObject.getSeconds();
  seconds = (seconds < 10 ? "0" : "") + seconds;

  return hour + ":" + minutes + ":" + seconds;
}

http.listen(3000, function(){
  console.log('INFO: Listening on *:3000...');
});
